# Dockerfile for dev
FROM node:latest
WORKDIR /web     # More on this in a bit
COPY package*.json ./
RUN npm install
COPY . ./
EXPOSE 8080


# Hopefully you'd never actually do this, just copy everything, including locally installed node_modules
CMD ["npm", "start"] 