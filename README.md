# Reconstruir los modules de NODE

`npm i`

# Ejecutar el Proyecto

`npx webpack-dev-server -D`

# En el index existen 2 URL para conectar al servidor.

Es el mismo servidor solo que para probar, lo tengo corriendo en un contenedor en una instancia AWS.

Instancia de Ubuntu en AWS corriendo Contenedor Docker Node con Imagen Redis: `uri: "http://3.83.46.70:4000/graphql`
Local: `uri: "http://localhost:4000/graphql`