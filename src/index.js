import React from 'react';
import { render } from 'react-dom';

import { RootSession } from './App';
import ApolloClient, { InMemoryCache } from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// CONFIGURACION APOLLO CLIENT
const client = new ApolloClient({
    uri: "http://localhost:4000/graphql",

    // Enviar token al servidor
    fetchOptions: {
        credentials: 'include'
    },
    // para verificar que al navegar por mi aplicacion, el usuario este autenticado!
    // el request se ejecuta en cada pagina, verificando que tenga un token valido
    request: operation => {
        const token = localStorage.getItem('token')
        operation.setContext({
            headers: {
                autorization: token
            }
        });
    },
    cache: new InMemoryCache({
        addTypename: false
    }),
    onError: ({ networkError, graphQLErrors }) => {
        console.log('graphQLErrors', graphQLErrors);
        console.log('networkError', networkError);
    }
});
render(
    <ApolloProvider client={client}>
        <RootSession />
    </ApolloProvider>,
    document.getElementById('app')
);