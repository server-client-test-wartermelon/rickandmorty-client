import React, { Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import Session from './components/Session';
import Header from './components/Layout/Header';
import Registro from './components/Auth/Registro';
import Login from './components/Auth/Login';
import Panel from './components/Panel/Panel';

const App = ({ refetch, session }) => {

  const { obtenerUsuario } = session;

  const mensaje = (obtenerUsuario) ? `Bienvenido: ${obtenerUsuario.nombre}` : <Redirect to="/login" />;

  return (
    <Router>
      <Fragment>
        <Header session={session} />
        <div className="container">
          <p className="text-right">{mensaje}</p>
          <Switch>
            <Route exact path="/" render={() => <Redirect to="/login" />} />
            <Route exact path="/panel" render={() => <Panel session={session} />} />
            <Route exact path="/login" render={() => <Login refetch={refetch} session={session} />} />
            <Route exact path="/registro" render={() => <Registro session={session} />} />
            <Route path='*' exact={true} render={() => <Redirect to="/login" />} />
          </Switch>
        </div>
      </Fragment>
    </Router>
  );

}

const RootSession = Session(App);

export { RootSession } 