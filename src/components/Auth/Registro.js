import React, { Component, Fragment } from 'react';
import { Mutation } from 'react-apollo';
import { NUEVO_USUARIO } from '../../mutations';
import Error from '../Alertas/Error';
import { withRouter, Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';


const inicialState = {
  usuario: '',
  password: '',
  repetirPassword: '',
  nombre: '',
}

class Registro extends Component {

  // Redirijo al panel si el usuario ya esta autenticado
  componentDidMount() {
    if (this.props.session.obtenerUsuario !== null) {
      this.props.history.push('/panel');
    }
  }

  state = {
    ...inicialState
  }

  actualizarState = (e) => {
    const { name, value } = e.target;

    this.setState({
      [name]: value
    });
  }

  validarForm = () => {
    const patt = new RegExp(/^[A-Za-z0-9]+$/g);
    const { usuario, password, repetirPassword, nombre } = this.state;
    const noValido = !usuario || !password || !nombre || password !== repetirPassword || !patt.test(usuario);
    return noValido;
  }

  limpiarState = () => {
    this.setState({ ...inicialState })
  }

  crearRegistro = (e, crearUsuario) => {
    e.preventDefault();
    crearUsuario().then(data => {
      // console.log(data);
      this.limpiarState();
      //Redireccionamos al login
      this.props.history.push('/login');
    });
  }

  render() {
    const { usuario, password, repetirPassword, nombre } = this.state;

    return (
      <Fragment>
        <h1 className="text-center mb-5">Nuevo Usuario</h1>
        <div className="row  justify-content-center">

          <Mutation mutation={NUEVO_USUARIO} variables={{ usuario, password, nombre }}>

            {(crearUsuario, { loading, error, data }) => {

              return (
                <form className="col-md-8"
                  onSubmit={e => this.crearRegistro(e, crearUsuario)}>

                  {error && <Error error={error} />}
                  <div className="form-group">
                    <label>Usuario</label>
                    <input
                      onChange={this.actualizarState}
                      type="text"
                      name="usuario"
                      className="form-control"
                      placeholder="Usuario"
                      value={usuario}
                    />
                    <small className="form-text text-info">(Sin espacios y sin caracteres especiales)</small>
                  </div>
                  <div className="form-group">
                    <label>Nombre</label>
                    <input
                      onChange={this.actualizarState}
                      type="text"
                      name="nombre"
                      className="form-control"
                      placeholder="Nombre Completo"
                      value={nombre}
                    />
                    <small className="form-text text-muted">(Nombre y apellido Completos)</small>

                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-6">
                      <label>Password</label>
                      <input
                        onChange={this.actualizarState}
                        type="password"
                        name="password"
                        className="form-control"
                        placeholder="Password"
                        value={password}
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <label>Repetir Password</label>
                      <input
                        onChange={this.actualizarState}
                        type="password"
                        name="repetirPassword"
                        className="form-control"
                        placeholder="Repetir Password"
                        value={repetirPassword}
                      />
                    </div>
                  </div>

                  <button
                    disabled={loading || this.validarForm()}
                    type="submit"
                    className="btn btn-danger mr-1">
                    Crear Usuario
                  </button>
                  <Link to="/login" className="btn btn-warning float-right">Iniciar Sesion</Link>
                </form>
              );
            }}
          </Mutation>
        </div>
      </Fragment>
    )
  }
}

export default withRouter(Registro);
