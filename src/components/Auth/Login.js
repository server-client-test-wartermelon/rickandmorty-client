import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import Error from '../Alertas/Error';
import { Mutation } from 'react-apollo';
import { AUTENTICAR_USUARIO } from '../../mutations';
import { Link } from 'react-router-dom';

const initialState = {
  usuario: '',
  password: ''
}

class Login extends Component {

  // Redirijo al panel si el usuario ya esta autenticado
  componentDidMount() {
    if (this.props.session.obtenerUsuario !== null) {
      this.props.history.push('/panel');
    }
  }

  state = {
    ...initialState
  }

  actualizarState = e => {
    const { name, value } = e.target;

    this.setState({
      [name]: value
    })
  }


  limpiarState = () => {
    this.setState({ ...initialState });
  }

  iniciarSesion = (e, usuarioAutenticar) => {

    e.preventDefault();

    usuarioAutenticar().then(async ({ data }) => {

      localStorage.setItem('token', data.autenticarUsuario.token);

      // ejecutar el query una vez que haya iniciado sesion
      // Este refetch existe en el query que esta en Session cuando consulta por el USUARIO_ACTUAL, ese Session redea a todos los componentes de la aplicacion, y le pasa el refetch por props, al App.js y desde el route me pasa por props el refetch a mi. o sea, al Login
      await this.props.refetch();
      // limpiar state
      this.limpiarState();

      // redimensionar
      // setTimeout(() => {
      this.props.history.push('/panel')
      // }, 500);
    });
  }

  validarForm = () => {
    const patt = new RegExp(/^[A-Za-z0-9]+$/g);
    const { usuario, password } = this.state;

    const noValido = !usuario || !password || !patt.test(usuario);

    // console.log(noValido);
    return noValido;
  }
  render() {

    const { usuario, password } = this.state;

    return (
      <Fragment>
        <h1 className="text-center mb-5">Iniciar Sesión</h1>
        <div className="row  justify-content-center">

          <Mutation
            mutation={AUTENTICAR_USUARIO}
            variables={{ usuario, password }}
          >
            {(usuarioAutenticar, { loading, error, data }) => {

              // console.log(data);
              

              return (

                <form
                  onSubmit={e => this.iniciarSesion(e, usuarioAutenticar)}
                  className="col-md-8"
                >

                  {error && <Error error={error} />}


                  <div className="form-group">
                    <label>Usuario</label>
                    <input
                      onChange={this.actualizarState}
                      value={usuario}
                      type="text"
                      name="usuario"
                      className="form-control"
                      placeholder="Nombre Usuario"
                    />
                  </div>
                  <div className="form-group">
                    <label>Password</label>
                    <input
                      onChange={this.actualizarState}
                      value={password}
                      type="password"
                      name="password"
                      className="form-control"
                      placeholder="Password"
                    />
                  </div>

                  <button
                    disabled={
                      loading || this.validarForm()
                    }
                    type="submit"
                    className="btn btn-danger mr-1">
                    Iniciar Sesión
                  </button>
                  <Link to="/registro" className="btn btn-warning float-right">Registrarse</Link>

                </form>
              )
            }}
          </Mutation>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(Login);