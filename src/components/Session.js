import React from 'react';
import { Query } from 'react-apollo';
import { USUARIO_ACTUAL } from '../queries';

const Session = Component => props => (

  <Query query={USUARIO_ACTUAL}>
    {({ loading, error, data, refetch }) => {
      // COLOCO COMPONENT PORQUE ESTE HIGH ORDEN COMPONENT VA A RODEAR A TODOS LOS COMPONENTES QUE ESTAN EN EL SWITCH DE APP.JS SE LE VAN A IR PASANDO TODOS ESOS COMPONENTES HACIA EL SESION Y VA A VERIFICAR QUE EL USUARIO ESTA LOGEADO SINO NO VA A PERMITER QUE VEA NADA DEL AINFORMACION

      // para evitar posibles errores que podamos tener
      if (loading) return null;
      // pasamos los props a cada uno de los componentes
      // pasamos el refetch y la data que es la respuesta del servidor 
      // este Component es el App que esta en App.js
      console.log(data);
      
      return <Component {...props} refetch={refetch} session={data} />

    }}
  </Query>
)

export default Session;
