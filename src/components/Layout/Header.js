import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import CerrarSession from './CerrarSesion';
// import BotonRegistro from './BotonRegistro';

const Header = ({ session }) => {

  let barra = (session.obtenerUsuario) ? <NavegacionAutenticado session={session} /> : <NavegacionNoAutenticado />

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary justify-content-between d-flex mb-4">
      <div className="container">
        {barra}
      </div>
    </nav>
  )
};

const NavegacionNoAutenticado = () => (
  <Link to="/" className="navbar-brand text-light font-weight-bold">Rick and Morty Client</Link>
);

const NavegacionAutenticado = (session) => (
  <Fragment>
    <Link to="/" className="navbar-brand text-light font-weight-bold">Rick and Morty Client</Link>
    <CerrarSession />
    {/* <BotonRegistro session={session} /> */}
  </Fragment>
);

export default Header;