import React from 'react';
import { ApolloConsumer } from 'react-apollo';
import { withRouter } from 'react-router-dom';

const cerrarSesionUsuario = (cliente, history) => {
  localStorage.removeItem('token', '');

  // Se encarga de cachear todas las consultas hechas por apollo.. LIMPIA
  cliente.resetStore();

  //Redireccionar 
  history.push('/login');

}

const CerrarSesion = ({ history }) => (
  <ApolloConsumer>
    {cliente => {
      return (
        <button className="btn btn-light ml-md-2 mt-2 mt-md-0" onClick={() => cerrarSesionUsuario(cliente, history)}>
          Cerrar Sesion
      </button>
      )
    }}
  </ApolloConsumer>
)

export default withRouter(CerrarSesion);
