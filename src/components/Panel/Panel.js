import React, { Fragment, Component } from 'react';
import { OBTENER_PERSONAJES } from '../../queries';
import { Query } from 'react-apollo';


class Panel extends Component {
  state = {
    page: 1
  };

  siguiente = async (refetch) => {

    let aumentarPage = this.state.page;
    aumentarPage = aumentarPage + 1;
    this.setState({
      page: aumentarPage
    });
    await refetch();
  }

  atras = async (refetch) => {

    let aumentarPage = this.state.page;
    aumentarPage = aumentarPage - 1;
    this.setState({
      page: aumentarPage
    });
    await refetch();
  }

  render() {

    const { page } = this.state;

    return (
      <Fragment>
        <h1 className="text-center my-5">Character's Rick and Morty Cliente</h1>

        <Query query={OBTENER_PERSONAJES} variables={{ value: String(page) }}>

          {({ loading, error, data, refetch }) => {
            if (loading) return (
              <div className="loader">Cargando...</div>
            )
            if (error) return `Error al cargar ${error.message}`;

            // console.log(data);

            return (
              <Fragment>
                <div className="row">
                  {data.obtenerCharacters.map(item => {

                    const { id, image, name, status, species, gender } = item;

                    return (
                      <div key={id} className="col-md-3">
                        <div className=" card mb-3">
                          <h3 className="card-header">{name}</h3>
                          <div className="card-body">
                            <h5 className="card-title">Especie:</h5>
                            <h6 className="card-subtitle text-muted">{species}</h6>
                          </div>
                          <img src={image} alt={name} />
                          <ul className="list-group list-group-flush">
                            <li className="list-group-item">
                              <h5 className="card-title">Genero:</h5>
                              <h6 className="card-subtitle text-muted">{gender}</h6>
                            </li>
                            <li className="list-group-item">
                              <h5 className="card-title">Nombre:</h5>
                              <h6 className="card-subtitle text-muted">{name}</h6>
                            </li>
                          </ul>
                          <div className="card-footer text-muted">
                            <h5 className="card-title">Estatus:</h5>
                            <h6 className="card-subtitle text-muted">{status}</h6>
                          </div>
                        </div>
                      </div>
                    );

                  })}
                </div>
                <div className="d-flex justify-content-center mb-3">
                <button disabled={page === 1} className="btn btn-outline-primary" onClick={() => this.atras(refetch)}>
                  Atras
                </button>
                <button disabled={page === 25} className="btn btn-outline-primary ml-2" onClick={() => this.siguiente(refetch)}>
                  Siguiente
                </button>
                </div>

              </Fragment>
            );

          }}

        </Query>
      </Fragment>
    )
  }
}

export default Panel;