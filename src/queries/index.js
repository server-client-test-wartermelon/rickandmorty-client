import gql from "graphql-tag";

// USUARIOS
export const USUARIO_ACTUAL = gql`
query obtenerUsuario{
  obtenerUsuario{
    id
    usuario
    nombre
  }
}`;

// Solicito al Server los Personajes una vez autenticado correctamente
export const OBTENER_PERSONAJES = gql`
query	obtenerCharacters($value: String){
  obtenerCharacters(value: $value){
    id
    name
    species
    status
    gender
    image
  }
}`;